
#!/usr/bin/env bash

: ------------------------------------------
:   Introduction
: ------------------------------------------

# This script allows you to install the latest version of the
# "firebase" command by running:
#
:   curl -sL firebase.tools | bash
#
# If you do not want to use this script, you can manually
# download the latest "firebase" binary.
#
:   curl -Lo ./firebase_bin https://firebase.tools/bin/linux/latest
#
# Alternatively, you can download a specific version.
#
:   curl -Lo ./firebase_bin https://firebase.tools/bin/linux/v7.2.2
#
# Note: On Mac, replace "linux" with "macos" in the URL.
#
# For full details about installation options for the Firebase CLI
# please see our documentation.
#   https://firebase.google.com/docs/cli/
#
# Please report bugs / issues with this script on Github.
#   https://github.com/firebase/firebase-tools
#

: ------------------------------------------
:   Automated Script
: ------------------------------------------

# This script contains a large amount of comments so you can understand
# how it interacts with your system. If you're not interested in the
# technical details, you can safely skip this and just run the command above.

# We need to ensure that we don't mess up an existing "firebase"
# install, so before doing anything we check to see if this system
# has Node.js and/or "firebase" installed

echo "-- Checking for existing firebase-tools on PATH..."
HAS_FIREBASE_TOOLS=$(which firebase)

if [ ! -z "$HAS_FIREBASE_TOOLS" ]
then
    echo "Your machine already has firebase-tools@$(firebase --version) installed. Nothing to do."
    echo "-- All done!"
    exit 0
fi

echo "-- Checking your machine type..."

# At this point, we need to detect the platform we're running on
# (Linux / Mac / Other) so we can fetch the correct binary and place it in
# the correct location on the machine.

case "$(uname -s)" in
    Linux*)     MACHINE=linux;;
    Darwin*)    MACHINE=macos;;
esac
# Credit: https://stackoverflow.com/a/3466183

# If we never define the $MACHINE variable (because our platform is neither Mac
# or Linux), then we can't finish our job, so just log out a helpful message
# and close.
if [ -z "$MACHINE" ]
then
    echo "Your operating system is not supported, if you think it should be please file a bug."
    echo "https://github.com/firebase/firebase-tools/"
    echo "-- All done!"
    exit 0
fi

# We have enough information to generate the binary's download URL.

echo "-- Links..."
DOWNLOAD_URL="https://firebase.tools/bin/$MACHINE/latest"

echo "[Binary URL] $DOWNLOAD_URL"

# At this point, we can begin the download of the firebase-tools binary.
# We use "curl" with a flag set to follow redirects (Github download URLs redirect
# to CDNs) and a flag set to show a cute progress bar.

echo "-- Downloading binary..."
curl -o /tmp/firebase -L --progress-bar $DOWNLOAD_URL

# Once the download is complete, we mark the binary file as executable (+x)
# then run it once, asking it to print out the version. This is helpful as
# standalone firebase binaries do a small amount of setup on the initial run
# so this not only allows us to make sure we got the right version, but it
# also does the setup so the first time the developer runs the binary, it'll
# be faster

echo "-- Setting permissions on binary..."
chmod +x /tmp/firebase

INSTALL_DIR="/usr/local/bin/"
echo "-- Moving binary to $INSTALL_DIR..."
mv /tmp/firebase $INSTALL_DIR
# For info about why we place it at /usr/local/bin see https://unix.stackexchange.com/a/8658

echo "-- firebase-tools@$(firebase --version) is now installed at $(which firebase)"
echo "-- All Done!"

# ------------------------------------------
#   Notes
# ------------------------------------------
#
# This script contains hidden JavaScript which is used to improve
# readability in the browser (via syntax highlighting, etc), right-click
# and "View source" of this page to see the entire bash script!
#
# You'll also notice that we use the ":" character in the Introduction
# which allows our copy/paste commands to be syntax highlighted, but not
# ran. In bash : is equal to `true` and true can take infinite arguments
# while still returning true. This turns these commands into no-ops so
# when ran as a script, they're totally ignored.
#
#--Browser--#
# <script src="./readability.js"></script>
# <link href="https://cdnjs.cloudflare.com/ajax/libs/prism/1.16.0/themes/prism-okaidia.min.css" rel="stylesheet" />
# <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.16.0/components/prism-core.min.js"></script>
# <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.16.0/components/prism-bash.min.js"></script>
# <style>body {color: #272822; background-color: #272822; font-size: 0.8em;} </style>
